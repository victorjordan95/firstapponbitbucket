//Importa a classe Scanner para realizar leituras do teclado.
import java.util.Scanner;

public class fatorialRecursivo {

  /**
   * Este � um m�todo est�tico que faz o calculo do fatorial de um n�mero que �
   * passado como par�metro e retorna para a fun��o principal o resultado.
  */
  public static int fatorial(int num) {


      /**
       * Este � o caso base, se o n�mero passado por parametro for 0 ou 1,
       * ele retorna o resultado 1 e finaliza o m�todo.
       */
      if (num <= 1) {

          return 1;

      } else {

          /**
           * chama o m�todo fatorial novamente, mas dessa vez enviando como
           * parametro (n - 1).
           */
          
          return fatorial(num - 1) * num;

      }

  }

  public static void main(String[] args) {

      //Declara uma vari�vel para guardar o n�mero que o usu�rio digitar
      int numero;

      //Instancia um objeto da classe Scanner para realizar a leitura do teclado (System.in)
      Scanner entrada = new Scanner(System.in);

      //Imprime na sa�da
      System.out.println("Digite o n�mero que voc� pretende obter o fatorial.");

      /**Aqui a vari�vel 'numero' ir� receber a entrada que o usu�rio digitar.
       * A minha instancia de Scanner, no caso 'entrada', utilizar� o m�todo nextInt()
       * para ler o que vier do teclado como sendo um int.
       */
      numero = entrada.nextInt();

      //imprime o resultado do fatorial
      System.out.println("O fatorial de " + numero + " � " + fatorial(numero) + ".");


  }
}